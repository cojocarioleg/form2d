#import java.awt.*;

public class Form2D {
    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        jFrame.setSize(200,200);
        jFrame.setVisible(true);
        jFrame.add(new Board());

    }

}
    class Board extends JPanel{
    @Override
        public void paintComponent(Graphics g){
            super.paintComponents(g);
            draw1(g);
        }
        private void draw1(Graphics g){
            Graphics2D g2d = (Graphics2D)g;
            g2d.setPaint(Color.black);
            int w = getWidth();
            int h = getHeight();
            //g2d.drawLine(0, 0, 200, 200);
            //g2d.drawOval(50, 50, 100, 100);
            for (w = 0; w<= 200; w += 10){
                g2d.drawLine(0, w, 200, w);
            }
            for (h = 200; h>=0; h -= 10){
                g2d.drawLine(h, 0, h, 200
                );
            }

        }

    }